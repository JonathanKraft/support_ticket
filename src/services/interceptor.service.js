import { verifyAuth } from "@/services/authentication.service.js";
import router from '@/router';

axios.interceptors.request.use(function (config) {
    // Do something before request is sent
    if(!verifyAuth.verifyTokenValidity(localStorage.token)){
        router.replace({ name: "Login" });
    };
    return config;
  }, function (error) {
    // Do something with request error
    return Promise.reject(error);
  });

  /// THIS IS FOR JUST AND EXEMPLE, DO NOT KEEP AS SO

  const setupAxiosInterceptors = onUnauthenticated => {
    const onRequestSuccess = config => {
      console.log("request success", config);
      const token = Storage.local.get("auth");
      if (token) {
        config.headers.Authorization = `${token.token}`;
      }
      return config;
    };
    const onRequestFail = error => {
      console.log("request error", error);
      return Promise.reject(error);
    };
    axios.interceptors.request.use(onRequestSuccess, onRequestFail);
  
    const onResponseSuccess = response => {
      console.log("response success", response);
      return response;
    };
    const onResponseFail = error => {
      console.log("response error", error);
      const status = error.status || error.response.status;
      if (status === 403 || status === 401) {
        onUnauthenticated();
      }
      return Promise.reject(error);
    };
    axios.interceptors.response.use(onResponseSuccess, onResponseFail);
  };