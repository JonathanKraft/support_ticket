import axios from 'axios';
import { parseJwt } from "@/services/jwtdecoder.service.js";
import { Router } from "@/router.js";

export const HTTP = axios.create({
  baseURL: "http://support.project:8080/api",
  headers: {
    'Authorization': `Bearer ${localStorage.token}`,
    'Content-type': 'application/json',
  }
});

const router = Router;

export function logout() {
  delete localStorage.token;
  delete localStorage.username;
  delete localStorage.roles;
  this.$router.replace({ name: "Login" });
}

export const verifyAuth = {
  
  getToken() {
    return localStorage.token;
  },

  isLoggedIn() {
    let userToken = (this.getToken()) ? this.getToken() : false;
    return userToken;
  },

  getTokenExpirationDate(encodedToken) {
    let token = parseJwt(encodedToken);
    if (!token.exp) { return null; }

    let date = new Date(0);
    date.setUTCSeconds(token.exp);
    return date;
  },

  isTokenExpired(token) {
    let expirationDate = this.getTokenExpirationDate(token);
    return expirationDate > new Date();
  },

  verifyTokenValidity(token) {
    token = this.isLoggedIn();
    if (token) {
      return verifyAuth.isTokenExpired(token) 
    }
  }
}

