import Vue from 'vue'
//import Router from 'vue-router'
import Login from './views/Login.vue'
import Home from './views/Home.vue'
import VueRouter from 'vue-router';


Vue.use(VueRouter)

export const Router = new VueRouter({
  routes: [
    {
      path: '/',
      name: 'Login',
      component: Login
    },
    {
      path: '/404',
      //component: NotFound 
    },
    {
      path: '*',
      redirect: '/404'
    },
    {
      path: '/home',
      name: 'Home',
      component: Home,
      meta: { requiresAuth: true }
    },
  ]
});

Router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (!localStorage.token) {
      alert("Vous devez vous authentifier pour acceder à cette page");
    } else {
      next()
    }
  } else {
    next();
  }
});

export default Router;


////////////////////////////////////////////
////////////////////////////////////////////
////////////////////////////////////////////
////////////////////////////////////////////


// export default new Router({
//   routes: [
//     {
//       path: '/',
//       name: 'Login',
//       component: Login
//     },
//     {
//       path: '/home',
//       name: 'Home',
//       component: Home,
//       meta: { requiresAuth: true }
//     },
//   ],

//   beforeEach(to, from, next) {
//     if (to.matched.some(record => record.meta.requiresAuth)) {
//       alert("REQUIRES AUTH!");
//     } else {
//       next();
//     }
//   }
// });